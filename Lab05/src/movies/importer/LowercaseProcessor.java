//Mishal Alarifi 1942365
package movies.importer;

import java.util.ArrayList;


public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String srcDir, String dstDir){
		super(srcDir, dstDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> text){
		ArrayList<String> asLower = new ArrayList <String>();
		for(int i = 0; i < text.size(); i++){
			asLower.add(text.get(i).toLowerCase());
		}
		return asLower;
	}
	
}
