//Mishal Alarifi 1942365
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates (String srcDir, String dstDir){
		super(srcDir, dstDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> text){
		ArrayList<String> dupesRemoved = new ArrayList<String>();
		for(String line : text){
			if(!dupesRemoved.contains(line))
				dupesRemoved.add(line);
		}
		return dupesRemoved;
	}
}
